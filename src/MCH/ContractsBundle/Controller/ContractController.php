<?php

namespace MCH\ContractsBundle\Controller;

use MCH\ContractsBundle\Entity\TableOfDeposits;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use MCH\ContractsBundle\Entity\Contract;
use MCH\ContractsBundle\Entity\BuyerInformation;
use MCH\ContractsBundle\Entity\RealPropInf;
use MCH\ContractsBundle\Entity\PurchaseExp;
use MCH\ContractsBundle\Entity\ExplanationDep;
use MCH\ContractsBundle\Entity\BrokerInformation;
use MCH\ContractsBundle\Entity\RecipientOfDocuments;
use MCH\ContractsBundle\Form\ContractType;

/**
 * Contract controller.
 *
 */
class ContractController extends Controller
{

    /**
     * Lists all Contract entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MCHContractsBundle:Contract')->findAll();

        return $this->render('MCHContractsBundle:Contract:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new Contract entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Contract();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $deposits = $request->get('deposits');
        $documents = $request->get('documents');

        $data = $request->get('mch_contractsbundle_contract');
        $entity->getRealPropInf()->setModel($data['realPropInf']['model']);

        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();

        if (!empty($deposits)) {
            foreach ($deposits as $d) {
                $deposit = new TableOfDeposits();
                $deposit->setAmount($d['amount']);
                // $deposit->setCheckMadeOutTo($d['check_made_out_to']);
                $deposit->setCheckNumber($d['check_number']);
                $deposit->setContractId($entity);
                $deposit->setDateCollected($d['date_collected']);
                $deposit->setFor($d['for_']);
                $deposit->setPaymentType($d['payment_type']);
                $em->persist($deposit);
                $em->flush();
            }
        }

        if (!empty($documents)) {
            foreach ($documents as $d) {
                $document = new RecipientOfDocuments();
                $document->setText($d);
                $document->setContractId($entity);
                $em->persist($document);
                $em->flush();
            }
        }

        return $this->redirect($this->generateUrl('contract'));
    }

    /**
     * Creates a form to create a Contract entity.
     *
     * @param Contract $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Contract $entity)
    {
        $form = $this->createForm(new ContractType($this->getSalesRepresentativeChecklist(), $this->getDeposit(), $this->getDepositProperAccount()), $entity, array(
            'action' => $this->generateUrl('contract_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Contract entity.
     *
     */
    public function newAction()
    {
        $entity = new Contract();
        $entity->setBuyerInformation(new BuyerInformation());
        $form = $this->createCreateForm($entity);

        return $this->render('MCHContractsBundle:Contract:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Contract entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MCHContractsBundle:Contract')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Contract entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MCHContractsBundle:Contract:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Contract entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MCHContractsBundle:Contract')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Contract entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        $recipientOfDocuments = $em->getRepository('MCHContractsBundle:RecipientOfDocuments')->findByContractId($id);
        $documents = array();
        foreach ($recipientOfDocuments as $d) {
            $documents[] = $d->getText();
        }

        $tableOfDeposits = $em->getRepository('MCHContractsBundle:TableOfDeposits')->findByContractId($id);

        return $this->render('MCHContractsBundle:Contract:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'documents' => $documents,
            'tableOfDeposits' => $tableOfDeposits
        ));
    }

    /**
     * Creates a form to edit a Contract entity.
     *
     * @param Contract $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Contract $entity)
    {
        $form = $this->createForm(new ContractType($this->getSalesRepresentativeChecklist(), $this->getDeposit(), $this->getDepositProperAccount()), $entity, array(
            'action' => $this->generateUrl('contract_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Contract entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MCHContractsBundle:Contract')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Contract entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $deposits = $request->get('deposits');
        $documents = $request->get('documents');

        $data = $request->get('mch_contractsbundle_contract');
        $entity->getRealPropInf()->setModel($data['realPropInf']['model']);

        if (!empty($deposits)) {
            $query = $em->createQuery('DELETE FROM MCHContractsBundle:TableOfDeposits td WHERE td.contractId = :id ');
            $query->setParameter(':id', $id);
            $query->execute();

            foreach ($deposits as $d) {
                $deposit = new TableOfDeposits();
                $deposit->setAmount($d['amount']);
                // $deposit->setCheckMadeOutTo($d['check_made_out_to']);
                $deposit->setCheckNumber($d['check_number']);
                $deposit->setContractId($entity);
                $deposit->setDateCollected($d['date_collected']);
                $deposit->setFor($d['for_']);
                $deposit->setPaymentType($d['payment_type']);
                $em->persist($deposit);
            }
        }

        if (!empty($documents)) {
            $query = $em->createQuery('DELETE FROM MCHContractsBundle:RecipientOfDocuments rd WHERE rd.contractId = :id ');
            $query->setParameter(':id', $id);
            $query->execute();

            foreach ($documents as $d) {
                $document = new RecipientOfDocuments();
                $document->setText($d);
                $document->setContractId($entity);
                $em->persist($document);
            }
        }

        $em->persist($entity);
        $em->flush();

        $recipientOfDocuments = $em->getRepository('MCHContractsBundle:RecipientOfDocuments')->findByContractId($id);
        $documents = array();
        foreach ($recipientOfDocuments as $d) {
            $documents[] = $d->getText();
        }

        return $this->redirect($this->generateUrl('contract'));
    }

    /**
     * Deletes a Contract entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MCHContractsBundle:Contract')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Contract entity.');
            }

            // Removing contract dependencies
            $em->remove($entity->getBuyerInformation());
            $em->remove($entity->getRealPropInf());
            $em->remove($entity->getPurchaseExp());
            $em->remove($entity->getExplanationDep());
            $em->remove($entity->getBrokerInformation());
            $em->remove($entity->getIncentivesContributions());
            $em->remove($entity->getAddendumToModify());

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('contract'));
    }

    public function dropAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MCHContractsBundle:Contract')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Contract entity.');
        }

        // Removing contract dependencies
        $em->remove($entity->getBuyerInformation());
        $em->remove($entity->getRealPropInf());
        $em->remove($entity->getPurchaseExp());
        $em->remove($entity->getExplanationDep());
        $em->remove($entity->getBrokerInformation());
        $em->remove($entity->getIncentivesContributions());
        $em->remove($entity->getAddendumToModify());

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('contract'));
    }

    /**
     * Creates a form to delete a Contract entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('contract_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }

    public function exportPDFAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MCHContractsBundle:Contract')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Contract entity.');
        }

        //$entity->getBuyerInformation()->setSalesRepresentativeChecklist(json_decode($entity->getBuyerInformation()->getSalesRepresentativeChecklist()));

        $twigs['buyerInformation'] = 'MCHContractsBundle:Contract:buyerInformation.html.twig';
        // $twigs['memo'] = 'MCHContractsBundle:Contract:memo.html.twig';
        // $twigs['depositForm'] = 'MCHContractsBundle:Contract:depositForm.html.twig';
        // $twigs['noticeToBuyer'] = 'MCHContractsBundle:Contract:noticeToBuyer.html.twig';
        // $twigs['purchaseAgreement1'] = 'MCHContractsBundle:Contract:purchaseAgreement1.html.twig';
        // $twigs['purchaseAgreement2'] = 'MCHContractsBundle:Contract:purchaseAgreement2.html.twig';
        // $twigs['purchaseAgreement3'] = 'MCHContractsBundle:Contract:purchaseAgreement3.html.twig';
        // $twigs['purchaseAgreement4'] = 'MCHContractsBundle:Contract:purchaseAgreement4.html.twig';
        // $twigs['purchaseAgreement5'] = 'MCHContractsBundle:Contract:purchaseAgreement5.html.twig';
        // $twigs['purchaseAgreement6'] = 'MCHContractsBundle:Contract:purchaseAgreement6.html.twig';
        // $twigs['occupancy'] = 'MCHContractsBundle:Contract:occupancy.html.twig';
        // $twigs['realtorClientRegistration'] = 'MCHContractsBundle:Contract:realtorClientRegistration.html.twig';
        // $twigs['amendment'] = 'MCHContractsBundle:Contract:amendment.html.twig';
        // $twigs['broker'] = 'MCHContractsBundle:Contract:broker.html.twig';
        // $twigs['certification'] = 'MCHContractsBundle:Contract:certification.html.twig';
        // $twigs['builder'] = 'MCHContractsBundle:Contract:builder.html.twig';
        // $twigs['mold1'] = 'MCHContractsBundle:Contract:mold1.html.twig';
        // $twigs['mold2'] = 'MCHContractsBundle:Contract:mold2.html.twig';
        // $twigs['addendum'] = 'MCHContractsBundle:Contract:addendum.html.twig';
        // $twigs['fha'] = 'MCHContractsBundle:Contract:fha.html.twig';
        // $twigs['receiptOfDocuments'] = 'MCHContractsBundle:Contract:receiptOfDocuments.html.twig';

        $tableOfDeposits = $em->getRepository('MCHContractsBundle:TableOfDeposits')->findByContractId($id);

        $html = array();
        foreach ($twigs as $twig) {
            $html[] = $this->renderView($twig,
                array(
                    'contract' => $entity,
                    'checklist' => $this->getSalesRepresentativeChecklist(),
                    'deposit' => $this->getDeposit(),
                    'depositproperaccount' => $this->getDepositProperAccount(),
                    'tableOfDeposits' => $tableOfDeposits
                ));
        }

        $html_test = $this->render('MCHContractsBundle:Contract:buyerInformation.html.twig',
            array(
                'contract' => $entity,
                'checklist' => $this->getSalesRepresentativeChecklist(),
                'deposit' => $this->getDeposit(),
                'depositproperaccount' => $this->getDepositProperAccount(),
                'tableOfDeposits' => $tableOfDeposits
            ));

        // return new Response($html_test);

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html, array('page-size' => 'Legal', 'default-header' => false)),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="out.pdf"'
            )
        );
    }

    private function getSalesRepresentativeChecklist()
    {
        return array(
            '01' => 'Information Survey',
            '02' => 'Memo',
            '03' => 'Management approval email',
            '04' => 'Deposit Form',
            '05' => 'Buyer’s Drivers Licenses or other Government ID',
            '06' => 'Proof of Civil Servant',
            '07' => 'Notice to Buyer',
            '08' => 'Purchase Agreement',
            '09' => 'Occupancy Disclosure',
            '10' => 'CDD Notice',
            '11' => 'Taxing District',
            '12' => 'Escrow Disclosure',
            '13' => 'Realtor Client Registration',
            '13a' => 'Broker Participation Agreement',
            '14' => 'Amendment I to Realtor Client Registration From',
            '14a' => 'Addendum to Broker Agreement',
            '15' => 'Copy of Agent’s Business Card',
            '16' => 'Broker Rebate Certification',
            '17' => 'Broker Rebate Addendum',
            '18' => 'Community Addendum',
            '19' => 'Certification of Insulation',
            '20' => 'Sellers Affiliation',
            '21' => 'Builder’s Referral Incentives & Contributions',
            '23' => 'Tri-fold or Pre-Approval',
            '24' => 'Options Addendum',
            '25' => 'Appliance Sheet',
            '26' => 'Mold Disclosure, Disclaimer and Waiver',
            '27' => 'Design Checklist',
            '28' => 'Alarm Service',
            '29' => 'Addendum to Modify Purchase Agreement',
            '30' => 'FHA/VA Addendum',
            '31' => 'Completed Home Addendum',
            '32' => 'Price Protection Guarantee',
            '33' => 'Model Addendum',
            '34' => 'EFBD Disclosure Statement',
            '35' => 'Agricultural Addendum',
            '36' => 'Receipts of Documents',
            '37' => 'Preparing for your design appointment',
            '38' => 'Site Plan Addendum for Homeowner Approval'
        );
    }

    private function getDeposit()
    {
        return array(
            'initial' => 'Initial Deposit',
            'additional' => 'Additional Deposit',
            'design' => 'Design Deposit',
        );
    }

    private function getDepositProperAccount()
    {
        return array(
            'MCH' => 'Mountain Cove Homes at South Dade, LLC',
            'LOAG' => 'Law Offices of Alexis Gonzalez, PA Trust Account',
            'Other' => 'Other'
        );
    }

    public function modelCommunityAjaxAction($key)
    {
        $list = array(
            'cutler_bay' => array(
                'empty_value' => 'Choose a Model',
                'paradise_palm' => 'Paradise Palm',
                'queen_palm' => 'Queen Palm'
            ),
            'tamiami' => array(
                'empty_value' => 'Choose a Model',
                'emerald_palm' => 'Emerald Palm'
            ),
            'south_dade' => array(
                'empty_value' => 'Choose a Model',
                'bonsai_palm' => 'Bonsai Palm',
                'cascade_palm' => 'Cascade Palm',
                'grand_palm' => 'Grand Palm',
                'queen_palm' => 'Queen Palm'
            ),
            'south_dade_ii' => array(
                'empty_value' => 'Choose a Model',
                'bismark_palm' => 'Bismark Palm',
                'coconut_palm' => 'Coconut Palm',
                'date_palm_1_car_garage' => 'Date Palm 1 Car Garage',
                'date_palm_2_car_garage' => 'Date Palm 2 Car Garage',
                'everglades_palm' => 'Everglades Palm'
            ),
            'villa_catalina_i' => array(
                'empty_value' => 'Choose a Model',
                'villa_catalina_i' => 'Villa Catalina I'
            )
        );

        $json = null;
        if (isset($list[$key])) {
            foreach ($list[$key] as $key => $item) {
                $i = array('id' => $key, 'value' => $item);
                $json[] = $i;
            }
        }
        $json = json_encode($json);

        $response = new Response($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function modelCommunityIDAjaxAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MCHContractsBundle:RealPropInf')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Contract entity.');
        }

        $json = array('id' => $id, 'value' => $entity->getModel());

        $json = json_encode($json);

        $response = new Response($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

}
