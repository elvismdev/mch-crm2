<?php

namespace MCH\ContractsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataTransformer\BaseDateTimeTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ContractType extends AbstractType
{
    private $checklist;
    private $deposit;
    private $depositproperAccount;

    function __construct($checklist=null, $deposit=null, $depositproperAccount=null)
    {
        $this->checklist = $checklist;
        $this->deposit = $deposit;
        $this->depositproperAccount = $depositproperAccount;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', 'text', array('data' => date('m/d/Y')))
            ->add('buyerInformation', new BuyerInformationType())
            ->add('realPropInf', new RealPropInfType())
            ->add('purchaseExp', new PurchaseExpType())
            ->add('explanationDep', new ExplanationDepType())
            ->add('brokerInformation', new BrokerInformationType())
            ->add('incentivesContributions', new IncentivesAndContributionsType())
            ->add('addendumToModify', new AddendumToModifyType())
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MCH\ContractsBundle\Entity\Contract'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mch_contractsbundle_contract';
    }
}
