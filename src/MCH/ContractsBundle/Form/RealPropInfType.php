<?php

namespace MCH\ContractsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RealPropInfType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('salesRep')
            ->add('streetAddress')
            ->add('lot')
            ->add('block')
            ->add('subdivision')
            ->add('platBook')
            ->add('platBookPage')
            ->add('county', 'choice', array(
                    'empty_value' => 'Choose a County',
                    'choices' => array(
                        'miami_dade' => 'Miami Dade'
                    )
                )
            )
            ->add('mchCommunity', 'choice', array(
                    'empty_value' => 'Choose a Community',
                    'choices' => array(
                        'Blue Palms' => array(
                            'cutler_bay' => 'Blue Palms at Cutler Bay',
                            'tamiami' => 'Blue Palms at Tamiami',
                            'south_dade' => 'Blue Palms at South Dade',
                            'south_dade_ii' => 'Blue Palms at South Dade II'
                        ),
                        'The Village Collection' => array(
                            'villa_catalina_i' => 'Villa Catalina I'
                        )
                    )
                )
            )
            ->add('model', 'choice', array(
                    'empty_value' => 'Choose a Model',
                    'choices' => array()
                )
            )

            ->add('elevation')
            ->add('garageOr', 'choice', array(
                    'choices' => array(
                        'l' => 'Left',
                        'r' => 'Right'
                    ),
                    'expanded' => true
                )
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MCH\ContractsBundle\Entity\RealPropInf'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mch_contractsbundle_realpropinf';
    }
}
