<?php

namespace MCH\ContractsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class IncentivesAndContributionsType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titleAndSettlementIncentiveAmount')
            ->add('titleAndSettlementIncentivePercentage')
            ->add('closingCostIncentiveAmount')
            ->add('closingCostIncentivePercentage')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MCH\ContractsBundle\Entity\IncentivesAndContributions'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mch_contractsbundle_incentivesandcontributions';
    }
}
