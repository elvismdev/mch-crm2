<?php

namespace MCH\ContractsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class RecipientOfDocuments {
    private $id;

    private $text;

    private $contractId;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $contractId
     */
    public function setContractId(\MCH\ContractsBundle\Entity\Contract $contractId = null)
    {
        $this->contractId = $contractId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContractId()
    {
        return $this->contractId;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }
} 