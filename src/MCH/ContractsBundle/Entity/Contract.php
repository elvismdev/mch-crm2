<?php

namespace MCH\ContractsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contract
 */
class Contract
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var \MCH\ContractsBundle\Entity\BuyerInformation
     */
    private $buyerInformation;

    /**
     * @var \MCH\ContractsBundle\Entity\RealPropInf
     */
    private $realPropInf;

    /**
     * @var \MCH\ContractsBundle\Entity\PurchaseExp
     */
    private $purchaseExp;

    /**
     * @var \MCH\ContractsBundle\Entity\ExplanationDep
     */
    private $explanationDep;

    /**
     * @var \MCH\ContractsBundle\Entity\BrokerInformation
     */
    private $brokerInformation;

    /**
     * @var \MCH\ContractsBundle\Entity\IncentivesAndContributions
     */
    private $incentivesContributions;

    /**
     * @var \MCH\ContractsBundle\Entity\AddendumToModify
     */
    private $addendumToModify;

    /**
     * Set id
     *
     * @param integer $id
     * @return Contract
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Contract
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set buyerInformation
     *
     * @param \MCH\ContractsBundle\Entity\BuyerInformation $buyerInformation
     * @return Contract
     */
    public function setBuyerInformation(\MCH\ContractsBundle\Entity\BuyerInformation $buyerInformation = null)
    {
        $this->buyerInformation = $buyerInformation;

        return $this;
    }

    /**
     * Get buyerInformation
     *
     * @return \MCH\ContractsBundle\Entity\BuyerInformation
     */
    public function getBuyerInformation()
    {
        return $this->buyerInformation;
    }

    /**
     * Set realPropInf
     *
     * @param \MCH\ContractsBundle\Entity\RealPropInf $realPropInf
     * @return Contract
     */
    public function setRealPropInf(\MCH\ContractsBundle\Entity\RealPropInf $realPropInf = null)
    {
        $this->realPropInf = $realPropInf;

        return $this;
    }

    /**
     * Get realPropInf
     *
     * @return \MCH\ContractsBundle\Entity\RealPropInf
     */
    public function getRealPropInf()
    {
        return $this->realPropInf;
    }

    /**
     * Set purchaseExp
     *
     * @param \MCH\ContractsBundle\Entity\PurchaseExp $purchaseExp
     * @return Contract
     */
    public function setPurchaseExp(\MCH\ContractsBundle\Entity\PurchaseExp $purchaseExp = null)
    {
        $this->purchaseExp = $purchaseExp;

        return $this;
    }

    /**
     * Get purchaseExp
     *
     * @return \MCH\ContractsBundle\Entity\PurchaseExp
     */
    public function getPurchaseExp()
    {
        return $this->purchaseExp;
    }


    /**
     * Set explanationDep
     *
     * @param \MCH\ContractsBundle\Entity\ExplanationDep $explanationDep
     * @return Contract
     */
    public function setExplanationDep(\MCH\ContractsBundle\Entity\ExplanationDep $explanationDep = null)
    {
        $this->explanationDep = $explanationDep;

        return $this;
    }

    /**
     * Get explanationDep
     *
     * @return \MCH\ContractsBundle\Entity\explanationDep
     */
    public function getExplanationDep()
    {
        return $this->explanationDep;
    }


    /**
     * Set brokerInformation
     *
     * @param \MCH\ContractsBundle\Entity\BrokerInformation $brokerInformation
     * @return Contract
     */
    public function setBrokerInformation(\MCH\ContractsBundle\Entity\BrokerInformation $brokerInformation = null)
    {
        $this->brokerInformation = $brokerInformation;

        return $this;
    }

    /**
     * Get brokerInformation
     *
     * @return \MCH\ContractsBundle\Entity\brokerInformation
     */
    public function getBrokerInformation()
    {
        return $this->brokerInformation;
    }

    /**
     * Set incentivesContributions
     *
     * @param \MCH\ContractsBundle\Entity\IncentivesAndContributions $incentivesContributions
     * @return Contract
     */
    public function setIncentivesContributions(\MCH\ContractsBundle\Entity\IncentivesAndContributions $incentivesContributions = null)
    {
        $this->incentivesContributions = $incentivesContributions;

        return $this;
    }

    /**
     * Get incentivesContributions
     *
     * @return \MCH\ContractsBundle\Entity\IncentivesAndContributions
     */
    public function getIncentivesContributions()
    {
        return $this->incentivesContributions;
    }

    /**
     * Set addendumToModify
     *
     * @param \MCH\ContractsBundle\Entity\AddendumToModify $addendumToModify
     * @return Contract
     */
    public function setAddendumToModify(\MCH\ContractsBundle\Entity\AddendumToModify $addendumToModify = null)
    {
        $this->addendumToModify = $addendumToModify;

        return $this;
    }

    /**
     * Get addendumToModify
     *
     * @return \MCH\ContractsBundle\Entity\AddendumToModify
     */
    public function getAddendumToModify()
    {
        return $this->addendumToModify;
    }
}