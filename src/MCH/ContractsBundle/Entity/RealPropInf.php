<?php

namespace MCH\ContractsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RealPropInf
 */
class RealPropInf
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $salesRep;

    /**
     * @var string
     */
    private $streetAddress;

    /**
     * @var integer
     */
    private $lot;

    /**
     * @var integer
     */
    private $block;

    /**
     * @var string
     */
    private $subdivision;

    /**
     * @var string
     */
    private $platBook;

    /**
     * @var string
     */
    private $platBookPage;

    /**
     * @var string
     */
    private $county;

    /**
     * @var string
     */
    private $mchCommunity;

    /**
     * @var string
     */
    private $model;

    /**
     * @var integer
     */
    private $elevation;

    /**
     * @var string
     */
    private $garageOr;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set salesRep
     *
     * @param string $salesRep
     * @return RealPropInf
     */
    public function setSalesRep($salesRep)
    {
        $this->salesRep = $salesRep;

        return $this;
    }

    /**
     * Get salesRep
     *
     * @return string
     */
    public function getSalesRep()
    {
        return $this->salesRep;
    }

    /**
     * Set streetAddress
     *
     * @param string $streetAddress
     * @return RealPropInf
     */
    public function setStreetAddress($streetAddress)
    {
        $this->streetAddress = $streetAddress;

        return $this;
    }

    /**
     * Get streetAddress
     *
     * @return string
     */
    public function getStreetAddress()
    {
        return $this->streetAddress;
    }

    /**
     * Set lot
     *
     * @param integer $lot
     * @return RealPropInf
     */
    public function setLot($lot)
    {
        $this->lot = $lot;

        return $this;
    }

    /**
     * Get lot
     *
     * @return integer
     */
    public function getLot()
    {
        return $this->lot;
    }

    /**
     * Set block
     *
     * @param integer $block
     * @return RealPropInf
     */
    public function setBlock($block)
    {
        $this->block = $block;

        return $this;
    }

    /**
     * Get block
     *
     * @return integer
     */
    public function getBlock()
    {
        return $this->block;
    }

    /**
     * Set subdivision
     *
     * @param string $subdivision
     * @return RealPropInf
     */
    public function setSubdivision($subdivision)
    {
        $this->subdivision = $subdivision;

        return $this;
    }

    /**
     * Get subdivision
     *
     * @return string
     */
    public function getSubdivision()
    {
        return $this->subdivision;
    }

    /**
     * Set platBook
     *
     * @param string $platBook
     * @return RealPropInf
     */
    public function setPlatBook($platBook)
    {
        $this->platBook = $platBook;

        return $this;
    }

    /**
     * Get platBook
     *
     * @return string
     */
    public function getPlatBook()
    {
        return $this->platBook;
    }

    /**
     * Set platBookPage
     *
     * @param string $platBookPage
     * @return RealPropInf
     */
    public function setPlatBookPage($platBookPage)
    {
        $this->platBookPage = $platBookPage;

        return $this;
    }

    /**
     * Get platBookPage
     *
     * @return string
     */
    public function getPlatBookPage()
    {
        return $this->platBookPage;
    }

    /**
     * Set county
     *
     * @param string $county
     * @return RealPropInf
     */
    public function setCounty($county)
    {
        $this->county = $county;

        return $this;
    }

    /**
     * Get county
     *
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * Set mchCommunity
     *
     * @param string $mchCommunity
     * @return RealPropInf
     */
    public function setMchCommunity($mchCommunity)
    {
        $this->mchCommunity = $mchCommunity;

        return $this;
    }

    /**
     * Get mchCommunity
     *
     * @return string
     */
    public function getMchCommunity()
    {
        return $this->mchCommunity;
    }

    /**
     * Set model
     *
     * @param string $model
     * @return RealPropInf
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set elevation
     *
     * @param integer $elevation
     * @return RealPropInf
     */
    public function setElevation($elevation)
    {
        $this->elevation = $elevation;

        return $this;
    }

    /**
     * Get elevation
     *
     * @return integer
     */
    public function getElevation()
    {
        return $this->elevation;
    }

    /**
     * Set garageOr
     *
     * @param string $garageOr
     * @return RealPropInf
     */
    public function setGarageOr($garageOr)
    {
        $this->garageOr = $garageOr;

        return $this;
    }

    /**
     * Get garageOr
     *
     * @return string
     */
    public function getGarageOr()
    {
        return $this->garageOr;
    }

}
